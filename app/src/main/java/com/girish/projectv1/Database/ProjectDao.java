package com.girish.projectv1.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public interface ProjectDao {

    @Query("select * from project")
    List<Project> loadAllData();
}

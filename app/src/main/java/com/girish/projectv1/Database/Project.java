package com.girish.projectv1.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity
class Project {
    @PrimaryKey
    @NonNull
    public String id;

    public String projectName;

    public String dateCreation;

    public String projectId;

    public String dateUpdate;
}

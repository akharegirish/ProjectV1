package com.girish.projectv1.Fragments.Home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.girish.libraryv1.Base.BaseFragment;
import com.girish.libraryv1.Constants.UseM;
import com.girish.projectv1.R;

public class ClsHome extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            return LayoutInflater.from(getContext()).inflate(R.layout.home,container,false);
        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UseM.setTitle("Home",getActivity());
        view.findViewById(R.id.fbAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UseM.changeFragBs(new ClsAddForm(),null,getFragmentManager());
            }
        });
    }
}

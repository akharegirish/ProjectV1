package com.girish.libraryv1.Constants;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.girish.libraryv1.Constants.FragmentHandel.ClsCallFrg;
import com.girish.libraryv1.R;
import com.google.gson.Gson;

public class UseM {
    public static <S> S useGson(String response,Class<S> tClass){
        Gson gson = new Gson();
        return gson.fromJson(response,tClass);
    }

    public static void changeFrag(Fragment frag, Bundle b, FragmentManager fm){
        new ClsCallFrg().WithoutBackStack(fm,frag,b, R.id.container);
    }

    public static void changeFragBs(Fragment frag, Bundle b, FragmentManager fm){
        new ClsCallFrg().WithBackStack(fm,frag,b, R.id.container);
    }

    public static void setTitle(String title, FragmentActivity activity){
        try {
            activity.setTitle(title);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

package com.girish.libraryv1.Implements;

import java.util.HashMap;

/**
 * Created by Girish on 23/10/2017.
 */

public interface CallBackServerRes {
    void CallBackServer(String response, int key);
    void CallBackServer(String response, String key);
    void CallBackServer(String response, String key, HashMap<String, String> passedParams);
}

package com.girish.projectv1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.girish.libraryv1.Constants.UseM;
import com.girish.projectv1.Fragments.Home.ClsHome;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UseM.changeFrag(new ClsHome(),null,getSupportFragmentManager());
    }
}

package com.girish.libraryv1.Constants.FragmentHandel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


/**
 * Created by Girish on 16/10/2017.
 */

public class ClsCallFrg {

    public final void WithBackStack(FragmentManager fm, Fragment f, Bundle b, int i){
        ClsHandelFrag clsHandelFrag = new ClsHandelFrag(fm, f, b, i);
        clsHandelFrag.WithBackStack();
    }

    public final void WithoutBackStack(FragmentManager fm, Fragment f, Bundle b, int i){
        ClsHandelFrag clsHandelFrag = new ClsHandelFrag(fm, f, b, i);
        clsHandelFrag.WithOutBackStack();
    }
}

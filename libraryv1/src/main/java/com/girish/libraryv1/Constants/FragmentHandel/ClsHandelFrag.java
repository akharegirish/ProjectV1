package com.girish.libraryv1.Constants.FragmentHandel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.girish.libraryv1.Constants.Constants;


/**
 * Created by Girish on 11/10/2017.
 */

public class ClsHandelFrag {

    private FragmentManager manager;
    private Fragment fragment;
    private Bundle bundle;
    private Integer frameLayout;

    ClsHandelFrag(FragmentManager fragmentManager, Fragment fragment, Bundle savedInstanceState, Integer frame_layout){
        this.fragment=fragment;
        this.manager=fragmentManager;
        this.bundle=savedInstanceState;
        this.frameLayout=frame_layout;
    }


    public void WithBackStack(){

        WithBackStack(true);

    }

    public void WithOutBackStack(){

        WithBackStack(false);

    }

    private void WithBackStack(boolean addBackStack){
        String backStateName = fragment.getClass().getName();

        Fragment currentFrag = manager.findFragmentById(frameLayout);
        if (Constants.IS_PRINT_LOG)
            Log.e("Current Fragment", "" + currentFrag);

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        int countFrag = manager.getBackStackEntryCount();


        if (Constants.IS_PRINT_LOG)
            Log.e("Count", "" + countFrag);
        if (addBackStack) {
            if (currentFrag != null && currentFrag.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
                return;
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        if (!fragmentPopped) {

            if (bundle != null) {
                fragment.setArguments(bundle);
            }

            if (addBackStack) {
                ft.replace(frameLayout, fragment);
                //Check To Add Or Not Fragment To BackStack
                ft.addToBackStack(backStateName);


            }else {
                ft.replace(frameLayout, fragment, backStateName);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            }

            ft.commit();
        }
        if (Constants.IS_PRINT_LOG)
            Log.e("Current Fragment", "" + currentFrag);
    }
}

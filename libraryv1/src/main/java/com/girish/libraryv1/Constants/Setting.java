package com.girish.libraryv1.Constants;

public class Setting {
    /**
     * Set Default Orientation If True then @IS_ORIENTATION_PORTRAIT Will work*/
    public static boolean SET_DEFAULT_ORIENTATION = true;
    /**
     * Set @SET_DEFAULT_ORIENTATION to true for applying @IS_ORIENTATION_PORTRAIT else it will apply landscape mode*/
    public static boolean IS_ORIENTATION_PORTRAIT = true;

    /**
     * Set Default False if you don't need this on every Base Activity*/
    public static boolean SET_DIALOG = true;
    public static boolean SET_SHARED_PREF = true;

    /**
     * Set Default False if you don't need this on every Base Fragment*/
    public static boolean SET_DIALOG_F = true;
    public static boolean SET_SHARED_PREF_F = true;
}

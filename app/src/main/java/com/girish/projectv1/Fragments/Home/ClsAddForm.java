package com.girish.projectv1.Fragments.Home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.girish.libraryv1.Base.BaseFragment;
import com.girish.projectv1.Database.AppDatabase;
import com.girish.projectv1.Database.User;
import com.girish.projectv1.Database.utils.DatabaseInitializer;
import com.girish.projectv1.R;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ClsAddForm extends BaseFragment {

    private AppDatabase mDb;
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            return LayoutInflater.from(getContext()).inflate(R.layout.add_form,container,false);
        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle("Fill Project Form");
        textView= view.findViewById(R.id.tvTitle);
        mDb = AppDatabase.getInMemoryDatabase(getActivity().getApplicationContext());
        populateDb();
        view.findViewById(R.id.fbAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });
        fetchData();
    }
    @Override
    public void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    private void populateDb() {
        DatabaseInitializer.populateSync(mDb);
    }
    private void addData() {

    }

    private void fetchData() {
        // Note: this kind of logic should not be in an activity.
        StringBuilder sb = new StringBuilder();
        List<User> youngUsers = mDb.userModel().findUsersYoungerThan(35);
        for (User youngUser : youngUsers) {
            sb.append(String.format(Locale.US,
                    "%s, %s (%d)\n", youngUser.lastName, youngUser.name, youngUser.age));
        }
        textView.setText(sb);
    }
}
